# SailPete

## 🚀 Link

- https://sailpete.vercel.app/

## 💻 About The Project

SailPete is a mock sailing company that offers various courses from beginners to more experienced sailors. The app is a mobile friendly full stack web application engineered with TypeScript, React for the front-end UI and TailwindCSS for all styling components. Featuring a dark mode setting for better UX and using FormSubmit to collect and manage information easily and effective. Deployed with the help of Vercel.

## 🛠 Built With

This page was build with the use of the following frameworks/libraries and tools:

- [TypeScript][ts-url]
- [React][react-url]
- [Vite][vite-url]
- [TailwindCSS][tailwindcss-url]
- [Hero Icons][heroicons-url]
- [Framer Motion][framermotion-url]
- [Form Submit][formsubmit-url]
- [Vercel][vercel-url]

## 🪪 Contact

Daniel Rotstein | daniel.rotstein17@gmail.com | +1 (415) 969-0605

[ts-url]: https://www.typescriptlang.org/
[react-url]: https://reactjs.org/
[vite-url]: https://vitejs.dev/
[tailwindcss-url]: https://tailwindcss.com/
[heroicons-url]: https://heroicons.com/
[framermotion-url]: https://www.framer.com/motion/
[formsubmit-url]: https://formsubmit.co/
[vercel-url]: https://vercel.com/
